﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    [Authorize(Roles = CommonFuntions.AdminRole)]
    public class TesimonialController : Controller
    {
        protected Notify notify { get; set; }
        private readonly TestimonialService testyService;
        public TesimonialController()
        {
            testyService = new TestimonialService();
        }
        [DbAuthorize, SessionExpired]
        // GET: Tesimonial
        public ActionResult Index()
        {
            return View(testyService.TestimonialList());
        }
        [DbAuthorize, SessionExpired]
        public ActionResult Create()
        {
            return View();
        }

        [DbAuthorize, SessionExpired]
        [HttpPost]
        public ActionResult Create(TestmonialModel model, HttpPostedFileBase image)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    if (ModelState.IsValid)
                    {

                        if (testyService.AddTestimonial(model,image,Server))
                        {
                            notify = new Notify(NotifyLevel.Success, "Testimonial Added successfully");
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Testimonial Already exits");
                            return View(model);
                        }

                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return View();
        }

        public ActionResult Edit(int id)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                return View(testyService.Gettestimonail(id));
            }
        }


        [HttpPost]
        public ActionResult Edit(TestmonialModel model, HttpPostedFileBase image)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                if (ModelState.IsValid)
                {
                    if (testyService.EditTestimonial(model,image,Server))
                    {
                        notify = new Notify(NotifyLevel.Success, "Testimonial Updated successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        notify = new Notify(NotifyLevel.Error, "Testimonial Already already exits");
                        return RedirectToAction("Index");
                    }
                }
                notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                return View(model);
            }
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (testyService.deleteTestimonial(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Testimonial Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Testimonial can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}
