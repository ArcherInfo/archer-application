﻿using Archer.Models;
using Archer.Models.sql;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace Archer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblTestimonial> tests = db.tblTestimonials.ToList();
                    List<TestmonialModel> modellist = new List<TestmonialModel>();
                    if (tests.Count > 0)
                    {
                        foreach (tblTestimonial test in tests)
                        {
                            TestmonialModel model = new TestmonialModel();
                            model.Id = test.Id;
                            model.Via = test.Via;
                            model.Name = test.Name;
                            model.Description = test.Description;
                            model.Image = test.Image;
                            modellist.Add(model);
                        }
                        return View(modellist);
                    }
                    return View();
                }
            }
            catch (Exception e)
            {
                return View();
            }
            return View();
        }
        public ActionResult OnlineCourses()
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblOnlineCourse> cources = db.tblOnlineCourses.ToList();
                    List<OnlineCourcesModel> modellist = new List<OnlineCourcesModel>();
                    if (cources.Count > 0)
                    {
                        foreach (tblOnlineCourse cource in cources)
                        {
                            OnlineCourcesModel model = new OnlineCourcesModel();
                            model.id = cource.id;
                            model.Postedon = cource.Postedon;
                            model.Info = cource.Info;
                            model.Image = cource.Image;
                            model.Name = cource.Name;
                            model.Ratings = cource.ratings;
                            modellist.Add(model);
                        }
                        return View(modellist);
                    }
                    return View();
                }
            }
            catch (Exception e)
            {
                return View();
            }
        }
        public ActionResult DetailCourse(int id)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblOnlineCourse cource = db.tblOnlineCourses.FirstOrDefault(x => x.id == id);
                    if (cource != null)
                    {
                        OnlineCourcesModel model = new OnlineCourcesModel();
                        model.id = cource.id;
                        model.Postedon = cource.Postedon;
                        model.Info = cource.Info;
                        model.Image = cource.Image;
                        model.Name = cource.Name;
                        model.Ratings = cource.ratings;
                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {
                return View();
            }
            return View();
        }
        public ActionResult Youtube()
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblSubject> subjects = db.tblSubjects.ToList();
                    if (subjects.Count > 0)
                    {
                        List<SubjectWithChannel> schannels = new List<SubjectWithChannel>();
                        foreach (tblSubject subject in subjects)
                        {

                            SubjectWithChannel channel1 = new SubjectWithChannel();
                            channel1.SubjectId = subject.IdSubject;
                            channel1.subjectName = subject.Name;
                            List<ChannelModel> modellist = new List<ChannelModel>();
                            List<tblYoutube> channels = db.tblYoutubes.Where(x => x.subject == subject.IdSubject).ToList();
                            if (channels.Count > 0)
                            {
                                foreach (tblYoutube channel in channels)
                                {
                                    ChannelModel model = new ChannelModel();
                                    model.id = channel.id;
                                    model.Title = channel.Title;
                                    model.YouTube_Link = channel.YouTube_Link;
                                    model.Subject = channel.subject;
                                    modellist.Add(model);
                                }
                            }
                            channel1.channels = modellist;
                            schannels.Add(channel1);

                        }
                        return View(schannels);

                    }

                }
            }
            catch (Exception e)
            {

            }
            return View();
          
        }
        public ActionResult ContactInfo()
        {
            try
            {
                using (ArcherEntities ds = new ArcherEntities())
                {
                    List<tblContact> fs = ds.tblContacts.ToList();
                    List<ContactModel> ar = new List<ContactModel>();
                    foreach(tblContact tbl in fs)
                    {
                        ContactModel cns = new ContactModel();
                        cns.email = tbl.Email;
                        cns.lname = tbl.LastName;
                        cns.msg = tbl.Message;
                        cns.name = tbl.FirstName;
                        cns.subject = tbl.Subject;
                        ar.Add(cns);
                    }
                    return View(ar);
                }
            }
            catch(Exception e)
            {

            }
            return View(); 
        }
        public ActionResult Projects()
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblProject> projects = db.tblProjects.ToList();
                    List<ProjectModel> modellist = new List<ProjectModel>();
                    if (projects.Count > 0)
                    {
                        foreach (tblProject project in projects)
                        {
                            ProjectModel model = new ProjectModel();
                            model.id = project.id;
                            model.Title = project.Title;
                            model.Heading = project.Heading;
                            model.Praraghraph = project.Praraghraph;
                            model.Image = project.Image;
                            modellist.Add(model);
                        }
                        return View(modellist);
                    }
                    return View();
                }
            }
            catch (Exception e)
            {
                return View();
            }
            return View();
        }
        public ActionResult Course()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult Formfill()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Formfill(ArcherModels model)
        {
         
            return View(model);
        }
        public ActionResult Gallary()
        {
            return View();
        }
        public ActionResult Aboutus()
        {
            return View();
        }
        public ActionResult Exam_Recruitment()
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblExam> exams = db.tblExams.ToList();
                    List<ExamModel> modellist = new List<ExamModel>();
                    if (exams.Count > 0)
                    {
                        foreach (tblExam exam in exams)
                        {
                            ExamModel model = new ExamModel();
                            model.id = exam.id;
                            model.info = exam.Info;
                            model.img = exam.ImageName;
                            modellist.Add(model);
                        }
                    }
                    return View(modellist);
                }
            }
            catch
            {
                return View();
            }
            return View();
        }
        public ActionResult DetailExamInfo()
        {
            return View();
        }
        public ActionResult Batchshedule()
        {
            return View();
        }
        public ActionResult Clang(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Cpp(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult DS(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Csharp(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Java(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Python(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Javat(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Jdbc(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Servlet(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Hibernate(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Spring(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Swing(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Android(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Ios(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Kotlin(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult ReactNative(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Xamrin(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Cordova(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Ionic(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Bigdata(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Hadoop(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Saas(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Tableau(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult HtmlDemo(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Css(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult JavaScripts(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Php(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Ajax(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Bootstrap(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Arduino(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Iot(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Rasberry(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Dbms(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Rdbms(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Sql(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Mysql(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public ActionResult Oracle(string course)
        {
            ArcherModels model = new ArcherModels();
            model.course = course;
            return View(model);
        }
        public  JsonResult Sendtoemail(string fname,string lname,string email,string subject,string message)
        {
            using (ArcherEntities db = new ArcherEntities())
            {
                tblContact contact = new tblContact();
                contact.Email = email;
                contact.FirstName = fname;
                contact.LastName = lname;
                contact.Subject = subject;
                contact.Message = message;
                db.tblContacts.Add(contact);
                db.SaveChanges();
                
            }
                message = "name:" + fname + " " + lname + "message" + message;
            bool result = false;
            result = SendEmail(fname,lname,"maneaj96@gmail.com",subject,message);
           
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public bool SendEmail(string fname, string lname, string email,string subject,string me)
        {
            try
            {
                string senderemail = System.Configuration.ConfigurationManager.AppSettings["senderemail"].ToString();
                string senderpassword = System.Configuration.ConfigurationManager.AppSettings["senderpassword"].ToString();
                SmtpClient client = new SmtpClient("Smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderemail, senderpassword);
                MailMessage mailmessage = new MailMessage(senderemail,email,subject,me);
                mailmessage.IsBodyHtml = true;
                mailmessage.BodyEncoding = UTF8Encoding.UTF8;
                client.Send(mailmessage);

                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        
    }
}