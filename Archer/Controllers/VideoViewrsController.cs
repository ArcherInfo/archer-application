﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    [Authorize(Roles = CommonFuntions.AdminRole)]
    public class VideoViewrsController : Controller
    {
        [DbAuthorize, SessionExpired]
        // GET: VideoViewrs
        public ActionResult Index()
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblVideoViewr> viewrs = db.tblVideoViewrs.ToList();
                    List<ViewersModel> modellist = new List<ViewersModel>();
                    if (viewrs.Count > 0)
                    {
                        foreach (tblVideoViewr viewr in viewrs)
                        {
                            ViewersModel model = new ViewersModel();
                            model.Name = viewr.Name;
                            model.Contact = viewr.Contact;
                            model.Email = viewr.Email;
                            modellist.Add(model);
                        }
                    }
                    return View(modellist);
                }
            }
            catch
            {
                return View();
            }
            return View();
        }

        [DbAuthorize, SessionExpired]
        public ActionResult Create(ViewersModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblVideoViewr viewr = new tblVideoViewr();
                    viewr.Name = model.Name;
                    viewr.Email = model.Email;
                    viewr.Contact = model.Contact;
                    db.tblVideoViewrs.Add(viewr);
                    db.SaveChanges();
                }
                    return RedirectToAction("Exam_Recruitment","Home");
            }
            catch
            {
                return View();
            }
        }

        // GET: VideoViewrs/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: VideoViewrs/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: VideoViewrs/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: VideoViewrs/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
