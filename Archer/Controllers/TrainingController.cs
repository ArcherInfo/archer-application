﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using Archer.Utilities;
namespace Archer.Controllers
{
    [Authorize(Roles = CommonFuntions.AdminRole)]
    public class TrainingController : Controller
    {
        protected Notify notify { get; set; }
        private readonly TrainingService trainingService;
        public TrainingController()
        {
            trainingService = new TrainingService();
        }

        // GET: Training
        [DbAuthorize, SessionExpired]
        public ActionResult Index()
        {
            return View(trainingService.TrainingList());
        }

        // GET: Training/Create
        [HttpGet]
        [DbAuthorize, SessionExpired]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Training/Create
        [HttpPost]
        [DbAuthorize, SessionExpired]
        public ActionResult Create(TrainingModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    if (ModelState.IsValid)
                    {

                        if (trainingService.AddTraining(model))
                        {
                            notify = new Notify(NotifyLevel.Success, "Training Added successfully");
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Training Already exits");
                            return View(model);
                        }

                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return View();
        }

        // GET: Training/Edit/5
        [HttpGet]
        [DbAuthorize, SessionExpired]
        public ActionResult Edit(int id)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                return View(trainingService.Gettraining(id));
            }

        }

        // POST: Training/Edit/5
        [HttpPost]
        [DbAuthorize, SessionExpired]
        public ActionResult Edit(TrainingModel model)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                if (ModelState.IsValid)
                {
                    if (trainingService.EditTraining(model))
                    {
                        notify = new Notify(NotifyLevel.Success, "Training Updated successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        notify = new Notify(NotifyLevel.Error, "Training Already already exits");
                        return RedirectToAction("Index");
                    }
                }
                notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                return View(model);
            }
        }

        // GET: Training/Delete/5
        [DbAuthorize, SessionExpired]
        public ActionResult Delete(int id)
        {

            try
            {
                if (trainingService.DeleteTraining(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Training Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Training can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View();
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}
