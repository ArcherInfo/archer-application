﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    public class OnlineExamController : Controller
    {
        protected Notify notify { get; set; }
        private readonly ExamService examService;
        public OnlineExamController()
        {
            examService = new ExamService();
        }
        [DbAuthorize, SessionExpired]
        // GET: OnlineExam
        public ActionResult Index()
        {
            return View(examService.ExamList());
        }

        // GET: OnlineExam/Details/5

        // GET: OnlineExam/Create
        [DbAuthorize, SessionExpired]
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: OnlineExam/Create
        [DbAuthorize, SessionExpired]
        [HttpPost]
        public ActionResult Create(ExamModel model, HttpPostedFileBase image)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    if (ModelState.IsValid)
                    {

                        if (examService.AddExam(model, image, Server))
                        {
                            notify = new Notify(NotifyLevel.Success, "Exam Detail Added successfully");
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Please try After Sometime");
                        }

                        notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                        return View(model);
                    }
                }
            }
            catch
            {

            }
            return View();
        }

        // GET: OnlineExam/Edit/5
        public ActionResult Edit(int id)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                return View(examService.GetExam(id));
            }
        }

        // POST: OnlineExam/Edit/5
        [HttpPost]
        public ActionResult Edit(ExamModel model, HttpPostedFileBase image)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                if (ModelState.IsValid)
                {
                    if (examService.EditExam(model, image, Server))
                    {
                        notify = new Notify(NotifyLevel.Success, "Exam Details Updated successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {


                        notify = new Notify(NotifyLevel.Error, "Exam  Already already exits");
                        return RedirectToAction("Index");
                    }
                }
                notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                return View(model);
            }
        }
        public ActionResult Delete(int id)
        {
            try
            {
                if (examService.deleteExam(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Exam Info Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Exam can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View();
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}
