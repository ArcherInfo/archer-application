﻿using System;
using System.Linq;
using System.Web.Mvc;
using Archer.Models;
using Archer.Models.sql;
using Archer.Helpers;
using Archer.Utilities;
using System.Web.Security;
using Newtonsoft.Json;
using System.Web;

namespace Archer.Controllers
{
    public class AccountController : Controller
    {
        private IAuthenticationService auth;
        public AccountController()
        {
            auth = new FormsAuthenticationService();
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult LoginWindow(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginWindow(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (ArcherEntities db = new ArcherEntities())
                    {
                        var password = HashPasswordGenerator.HashPassword(model.Password);
                        tblLogin user = db.tblLogins.FirstOrDefault(x => x.UserName == model.UserName && x.Password == password);
                        if (user != null)
                        {
                            if (user.UserName == "Student")
                            {
                                Session["role"] = 2;
                                FormsAuthentication.RedirectFromLoginPage(user.UserName, false);
                                auth.Login(user.Id,"Student",false);
                                return RedirectToAction("Create", "Student");
                            }
                            else
                            {
                                FormsAuthentication.RedirectFromLoginPage(user.UserName, false);
                                auth.Login(user.Id,"Admin",false);
                                Session["login"] = user.Id;
                                Session["role"] = 1;
                                return RedirectToAction("ContactInfo", "Home");
                            }
                        }
                        else
                        {
                            TempData["WrongPasss"] = "Please Enter Correct Username Or Password";
                        }
                    }
                }
                else { TempData["WrongPasss"] = "Please Enter Username Or Password"; }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("Account controller" + e.Message);
            }
            return View();
        }

        public ActionResult Logout()
        {
            auth.Logoff();
            Session["login"] = null;
            return RedirectToAction("LoginWindow", "Account");
        }
    }
}