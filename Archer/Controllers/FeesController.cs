﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    public class FeesController : Controller
    {
        // GET: Fees
        public ActionResult Index()
        {
            return View();
        }
        private readonly FeesService feesService;
        public FeesController()
        {
            feesService = new FeesService();
        }

        [DbAuthorize, SessionExpired]
        public ActionResult payfees(int id)
        {
            FeesModel fmodel = new FeesModel();
            using (ArcherEntities db = new ArcherEntities())
            {
                tblStudent student = db.tblStudents.FirstOrDefault(x => x.StudentId == id);
                List<SubjectModel> modellist = new List<SubjectModel>();
                if (student != null)
                {
                    if (!String.IsNullOrEmpty(student.Subject))
                    {
                        string allsubjects = student.Subject;
                        string[] arraySubject = allsubjects.Split('|');
                        decimal? totalfees = 0;
                        foreach (string str in arraySubject)
                        {
                            if (str.Length != 0)
                            {
                                SubjectModel model = new SubjectModel();
                                int subjectid = Convert.ToInt32(str);
                                tblSubject subject = db.tblSubjects.FirstOrDefault(x => x.IdSubject == subjectid);
                                if (subject != null)
                                {
                                    model.name = subject.Name;
                                    model.fees = subject.Fees;
                                    totalfees += subject.Fees;
                                }
                                modellist.Add(model);
                            }
                        }

                        fmodel.student = student.StudentId;
                        fmodel.studentnm = student.Name;
                        fmodel.subjects = modellist;
                        fmodel.totalFee = totalfees;
                        List<tblFee> fees = db.tblFees.Where(x => x.Student == student.StudentId).ToList();
                        decimal? rfees = 0;
                        if (fees.Count > 0)
                        {
                            foreach (tblFee fee in fees)
                            {
                                rfees = rfees + fee.PaidAmount;
                            }
                            fmodel.remainingfees = totalfees - rfees;
                        }
                        else
                            fmodel.remainingfees = totalfees;
                    }
                }
            }
            return View(fmodel);
        }

        // GET: Fees/Delete/5
        public ActionResult RemainingFees()
        {
            using (ArcherEntities db = new ArcherEntities())
            {
                ViewBag.Subjects = new SelectList(db.tblSubjects.ToList(), "IdSubject", "Name");
                return View(new FeesPagingModel { data = new List<FeesModel>() });
            }
        }
        public JsonResult GetFeesList(FormCollection form)
        {
            return Json(feesService.GetFeesList(Request.Form), JsonRequestBehavior.AllowGet);
        }
        // POST: Fees/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [DbAuthorize, SessionExpired]
        public ActionResult PayAmount(FeesModel model)
        {
            try
            {
                using (ArcherEntities ds = new ArcherEntities())
                {
                    tblFee fee = new tblFee();
                    fee.Student = model.student;
                    fee.PaidAmount = model.paidamount;
                    fee.Date = DateTime.Now;
                    fee.RemainingAmount = model.remainingfees - model.paidamount;
                    ds.tblFees.Add(fee);
                    ds.SaveChanges();
                    return RedirectToAction("Index", "Student");
                }
            }
            catch(Exception e)
            {

                return View();
            }
            return View();
        }
    }
}
