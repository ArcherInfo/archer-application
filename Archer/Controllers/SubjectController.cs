﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    public class SubjectController : Controller
    {
        protected Notify notify { get; set; }
        private readonly SubjectService subjectService;
        public SubjectController()
        {
            subjectService = new SubjectService();
        }
        [DbAuthorize, SessionExpired]
        // GET: Subject
        public ActionResult Index()
        {
            return View(subjectService.SubjectList());
        }
        [DbAuthorize, SessionExpired]
        // GET: Subject/Details/5
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [DbAuthorize, SessionExpired]
        // POST: Subject/Create
        [HttpPost]
        public ActionResult Create(SubjectModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    if (ModelState.IsValid)
                    {

                        if (subjectService.AddSubject(model))
                        {
                            notify = new Notify(NotifyLevel.Success, "Subject Added successfully");
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Subject Already available");
                            return View(model);
                        }

                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return View();
        }
        [HttpGet]
        // GET: Subject/Edit/5
        public ActionResult Edit(int id)
        {

            using (ArcherEntities arc = new ArcherEntities())
            {
                return View(subjectService.Getsubject(id));
            }


        }

        // POST: Subject/Edit/5
        [HttpPost]
        public ActionResult Edit(SubjectModel model)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                if (ModelState.IsValid)
                {
                    if (subjectService.EditSubject(model))
                    {
                        notify = new Notify(NotifyLevel.Success, "subject Updated successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        if (subjectService.Errors.Count > 0)
                        {
                            notify = new Notify(NotifyLevel.Error, subjectService.Errors["Subject"]);
                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Subject Already already exits");
                        }
                        return View(model);

                    }
                }
                notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                return View(model);
            }
        }

        // GET: Subject/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                if (subjectService.DeleteSubject(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Subject Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Subject can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View();
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}
