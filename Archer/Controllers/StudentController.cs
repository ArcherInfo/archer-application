﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    public class StudentController : Controller
    {
        protected Notify notify { get; set; }
        private readonly StudentService studentService;
        public StudentController()
        {
            studentService = new StudentService();
        }
        // GET: Student
        [DbAuthorize, SessionExpired]
        public ActionResult Index()
        {
            return View(new StudentPagingModel { data = new List<StudentModel>() });
        }

        // GET: Student/Details/5
        [DbAuthorize, SessionExpired]
        // GET: Student/Create
        public ActionResult Create()
        {
            using (ArcherEntities db = new ArcherEntities())
            {
                var FromSubjects = getSubjectList();
                StudentModel model = new StudentModel(FromSubjects);
                return View(model);
            }
        }
        [DbAuthorize, SessionExpired]
        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(StudentModel model, HttpPostedFileBase image)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {

                    ViewBag.Subjects = new SelectList(db.tblSubjects.ToList(), "IdSubject", "Name");
                    model.FromSubjects = getSubjectList();

                    model.ToSubjects = getSubjectByID(model.TSelectedSubjects);
                    if (ModelState.IsValid)
                    {

                        if (studentService.AddStudent(model, image, Server))
                        {
                            notify = new Notify(NotifyLevel.Success, "Student Added successfully");
                            return RedirectToAction("Create");

                        }
                        else
                        {
                            if(studentService.Errors.Count>0)
                            {
                                notify = new Notify(NotifyLevel.Error, studentService.Errors["Student"]);
                            }
                            else
                            {
                                notify = new Notify(NotifyLevel.Error, "Student Already already exits");
                            }
                            return View(model);

                        }

                        notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return View();
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblStudent student = db.tblStudents.FirstOrDefault(x=>x.StudentId ==id);
                    if (student!=null)
                    {
                        StudentModel model = studentService.GetStudent(id);
                        model.FromSubjects = getSubjectList();
                        model.ToSubjects = getSubjectByString(student.Subject);
                        return View(model);

                    }
                }
            }
            catch(Exception e)
            {
                return View();
            }
            return View();
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(StudentModel model, HttpPostedFileBase image)
         {
            using (ArcherEntities arc = new ArcherEntities())
            {
                if (ModelState.IsValid)
                {
                    if (studentService.EditStudent(model,image,Server))
                    {
                        notify = new Notify(NotifyLevel.Success, "Student Updated successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        if (studentService.Errors.Count > 0)
                        {
                            notify = new Notify(NotifyLevel.Error, studentService.Errors["Student"]);
                        }
                        else
                        {

                            notify = new Notify(NotifyLevel.Error, "Student  Already already exits");
                            return View(model);
                        }

                    }
                }
                notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                model.FromSubjects = getSubjectList();
                return View(model);
            }
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                if (studentService.deleteStudent(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Student Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Student can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View(); ;
        }

        

        public JsonResult GetSubject(string searchterm)
        {
            using (ArcherEntities db = new ArcherEntities())
            {
                var datalist = db.tblSubjects.ToList();
                if (searchterm != null)
                {
                    datalist = db.tblSubjects.Where(x=>x.Name.Contains(searchterm)).ToList();
                }
                var modifieddata = datalist.Select(x=>new { id=x.IdSubject ,text = x.Name});
                return Json(modifieddata, JsonRequestBehavior.AllowGet);
            }
        }
        
        public List<SubjectModel> getSubjectByString(string allsubjects)
        {
            List<int> intlist = new List<int>();
            if (!(String.IsNullOrEmpty(allsubjects)))
            {
                string[] splitedSubjectss = allsubjects.Split('|');
                foreach (string str in splitedSubjectss)
                {
                    if (!(String.IsNullOrEmpty(str)))
                    {
                        intlist.Add((Convert.ToInt32(str)));
                    }
                }
            }
            return getSubjectByID(intlist);
        }

        public List<SubjectModel> getSubjectList()
        {
            List<SubjectModel> modellist = new List<SubjectModel>();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblSubject> tests = db.tblSubjects.ToList();

                    if (tests.Count > 0)
                    {
                        foreach (tblSubject test in tests)
                        {
                            SubjectModel model = new SubjectModel();
                            model.id = test.IdSubject;
                            model.name = test.Name;
                            model.fees = test.Fees!=null?(Int32)test.Fees:0;
                            modellist.Add(model);
                        }
                        return modellist;
                    }
                }
                return modellist;
            }
            catch (Exception e)
            {
                return modellist;
            }
        }
        public List<SubjectModel> getSubjectByID(List<int> SubjectIds)
        {
            List<SubjectModel> lSubjects = new List<SubjectModel>();
            try
            {
                using (var db = new ArcherEntities())
                {
                    foreach (int id in SubjectIds)
                    {
                        tblSubject subject = db.tblSubjects.FirstOrDefault(x => x.IdSubject == id);
                        if (subject != null)
                        {
                            SubjectModel model = new SubjectModel();
                            model.id = subject.IdSubject;
                            model.name = subject.Name;
                            lSubjects.Add(model);
                        }
                    }
                    return lSubjects;
                }
            }
            catch (Exception e)
            {

                return lSubjects;
            }
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }

        [DbAuthorize, HttpPost, SessionExpired]
        public JsonResult CheckStudent(string no)
        {
            using (var db = new ArcherEntities())
            {
                var student = db.tblStudents.FirstOrDefault(x => x.MobileNo == no);
                if (student != null)
                {
                    return Json(1);
                }
                else
                {
                    return Json(0);
                }
            }
        }
        public JsonResult GetStudentList(FormCollection form)
        {
            return Json(studentService.GetStudentList(Request.Form), JsonRequestBehavior.AllowGet);
        }
    }
}
