﻿using Archer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Archer.Models.sql;

namespace Archer.Controllers
{
    public class ArcherController : Controller
    {
        // GET: Archer
        [DbAuthorize, SessionExpired]
        public ActionResult Index()
        {
            try
            {
                using (ArcherEntities ds = new ArcherEntities())
                {
                    List<tblEnquiry> fs = ds.tblEnquiries.ToList();
                    List<ArcherModels> ar = new List<ArcherModels>();
                    foreach (tblEnquiry en in fs)
                    {
                        ArcherModels ams = new ArcherModels();
                        ams.fname = en.FirstName;
                        ams.lname = en.LastName;
                        ams.email = en.Email;
                        ams.course = en.Course;
                        ams.add = en.Address;
                        ams.contact = en.Contact;
                        ar.Add(ams);

                    }
                    return View(ar);
                }
            }
            catch (Exception e)
            {

            }
            return View();
        }
        
        // GET: Archer/Create
        [HttpGet]
        [DbAuthorize, SessionExpired]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Archer/Create
        [HttpPost]
        [DbAuthorize, SessionExpired]
        public ActionResult Create(ArcherModels model)
        {
            try
            {
                using (ArcherEntities ds = new ArcherEntities())
                {

                    tblEnquiry form = new tblEnquiry();

                    form.FirstName = model.fname;
                    form.LastName = model.lname;
                    form.Email = model.email;
                    form.Address = model.add;
                    form.Contact = model.contact;
                    form.Course = model.course;
                    ds.tblEnquiries.Add(form);
                    ds.SaveChanges();
                }

            }
            catch (Exception e)
            {

            }
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public ActionResult Register(ArcherModels model)
        {

            try
            {
                using (ArcherEntities ds = new ArcherEntities())
                {

                    tblEnquiry form = new tblEnquiry();

                    form.FirstName = model.fname;
                    form.LastName = model.lname;
                    form.Email = model.email;
                    form.Address = model.add;
                    form.Contact = model.contact;
                    form.Course = model.course;
                    ds.tblEnquiries.Add(form);
                    ds.SaveChanges();
                }

            }
            catch (Exception e)
            {

            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult StudentEnquiry(ArcherModels model)
        {
            try
            {
                using (ArcherEntities ds = new ArcherEntities())
                {

                    tblEnquiry form = new tblEnquiry();

                    form.FirstName = model.fname;
                    form.LastName = model.lname;
                    form.Email = model.email;
                    form.Address = model.add;
                    form.Contact = model.contact;
                    form.Course = model.course;
                    ds.tblEnquiries.Add(form);
                    ds.SaveChanges();
                }

            }
            catch (Exception e)
            {

            }
            return RedirectToAction("Index", "Home");
        }
    }
}