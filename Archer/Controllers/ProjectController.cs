﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    public class ProjectController : Controller
    {
        protected Notify notify { get; set; }
        private readonly ProjectService projectService;
        public ProjectController()
        {
            projectService = new ProjectService();
        }
        [DbAuthorize, SessionExpired]
        public ActionResult Index()
        {
            return View(projectService.ProjectList());
        }
        [DbAuthorize, SessionExpired]
        public ActionResult Create()
        {
            return View();
        }

        [DbAuthorize, SessionExpired]
        [HttpPost]
        public ActionResult Create(ProjectModel model, HttpPostedFileBase image)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    if (ModelState.IsValid)
                    {

                        if (projectService.AddProject(model, image, Server))
                        {
                            notify = new Notify(NotifyLevel.Success, "Project Detail Added successfully");
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Please try After Sometime");
                        }

                        notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                        return View(model);
                    }
                }
            }
            catch(Exception e)
            {

            }
            return View();
        }

        public ActionResult Edit(int id)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                return View(projectService.GetProject(id));
            }
        }


        [HttpPost]
        public ActionResult Edit(ProjectModel model, HttpPostedFileBase image)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                if (ModelState.IsValid)
                {
                    if (projectService.EditProject(model, image, Server))
                    {
                        notify = new Notify(NotifyLevel.Success, "Project Details Updated successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {


                        notify = new Notify(NotifyLevel.Error, "Project  Already already exits");
                        return RedirectToAction("Index");
                    }
                }
                notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (projectService.deleteProject(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Project Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Project can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View();
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}
