﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    public class OnlineCourcesController : Controller
    {
        protected Notify notify { get; set; }
        private readonly OnlineCourseService courseService;
        public OnlineCourcesController()
        {
            courseService = new OnlineCourseService();
        }
        [DbAuthorize, SessionExpired]
        // GET: OnlineCources
        public ActionResult Index()
        {
            return View(courseService.CourseList());
        }
        [DbAuthorize, SessionExpired]
        public ActionResult Create()
        {
            return View();
        }
        [DbAuthorize, SessionExpired]
        // POST: OnlineCources/Create
        [HttpPost]
        public ActionResult Create(OnlineCourcesModel model, HttpPostedFileBase image)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                        if (ModelState.IsValid)
                        {

                            if (courseService.AddCourse(model,image,Server))
                            {
                                notify = new Notify(NotifyLevel.Success, "Course Added successfully");
                                return RedirectToAction("Index");

                            }
                            else
                            {
                                notify = new Notify(NotifyLevel.Error, "Please try After Sometime");
                            }

                            notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                            return View(model);
                        }
                }
            }
            catch
            {

            }
            return View();
        }

        // GET: OnlineCources/Edit/5
        public ActionResult Edit(int id)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                return View(courseService.GetCourse(id));
            }
        }

        // POST: OnlineCources/Edit/5
        [HttpPost]
        public ActionResult Edit(OnlineCourcesModel model, HttpPostedFileBase image)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                if (ModelState.IsValid)
                {
                    if (courseService.EditCourse(model,image,Server))
                    {
                        notify = new Notify(NotifyLevel.Success, "Course Updated successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {


                        notify = new Notify(NotifyLevel.Error, "Course Already already exits");
                        return RedirectToAction("Index");
                    }
                }
                notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                return View(model);
            }

        }
        public ActionResult Delete(int id)
        {
            try
            {
                if (courseService.deleteCourse(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Course Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "course can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View();
        }
       
        
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}
