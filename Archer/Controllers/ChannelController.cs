﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Services;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Archer.Controllers
{
    [Authorize(Roles = CommonFuntions.AdminRole)]
    public class ChannelController : Controller
    {
        protected Notify notify { get; set; }
        private readonly ChannelService channelService;
        public ChannelController()
        {
            channelService = new ChannelService();
        }
        [DbAuthorize, SessionExpired]
        public ActionResult Index()
        {
            return View(channelService.ChannelList());
        }
        [DbAuthorize, SessionExpired]
        public ActionResult Create()
        {
            using (ArcherEntities db = new ArcherEntities())
            {
                ViewBag.Subjects = new SelectList(db.tblSubjects.ToList(), "IdSubject", "Name");
            }
            return View();
        }
        [DbAuthorize, SessionExpired]
        [HttpPost]
        public ActionResult Create(ChannelModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    ViewBag.Subjects = new SelectList(db.tblSubjects.ToList(), "IdSubject", "Name");
                    {
                        if (ModelState.IsValid)
                        {

                            if (channelService.AddChannel(model))
                            {
                                notify = new Notify(NotifyLevel.Success, "Channel Add successfully");
                                return RedirectToAction("Index");

                            }
                            else
                            {
                                notify = new Notify(NotifyLevel.Error, "Please try After Sometime");
                            }

                            notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                            return View(model);
                        }
                    }
                }
            }
            catch
            {

            }
            return View();
        }

        public ActionResult Edit(int id)
        {
            using (ArcherEntities arc = new ArcherEntities())
            {
                ViewBag.Subjects = new SelectList(arc.tblSubjects.ToList(), "IdSubject", "Name");
                return View(channelService.Getchannel(id));
            }
        }

        // POST: Channel/Edit/5
        [HttpPost]
        public ActionResult Edit(ChannelModel model)
        {
            
                using (ArcherEntities arc = new ArcherEntities())
                {
                    ViewBag.Subjects = new SelectList(arc.tblSubjects.ToList(), "IdSubject", "Name");
                    if (ModelState.IsValid)
                    {
                        if (channelService.EditUser(model))
                        {
                            notify = new Notify(NotifyLevel.Success, "Channel Updated successfully");
                            return RedirectToAction("Index");
                        }
                        else
                        {


                            notify = new Notify(NotifyLevel.Error, "Channel Already already exits");
                            return RedirectToAction("Index");
                        }
                    }
                    notify = new Notify(NotifyLevel.Warning, "Please Correct the errors");
                    return View(model);
                }
            
        }
        // GET: Channel/Delete/5
        public ActionResult Delete(int id)
        {

            try
            {
                if (channelService.DeleteChannel(id))
                {
                    notify = new Notify(NotifyLevel.Success, "Channel Deleted successfully");
                    return RedirectToAction("Index");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Channel can not delete..because used in other functionaity");
                }
            }
            catch
            {
                notify = new Notify(NotifyLevel.Warning, "Something wrong Please try after sometime");
            }
            return View();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}

