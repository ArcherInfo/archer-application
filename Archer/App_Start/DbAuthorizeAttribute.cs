﻿//-----------------------------------------------------------------------
// <copyright file="DbAuthorizeAttribute.cs" company="Storefloor">
//     Copyright (c) Storefloor. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System.Web;
using System.Web.Mvc;

namespace Archer
{
    public class DbAuthorizeAttribute : AuthorizeAttribute
    {        
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated
                && filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new ContentResult();
                filterContext.HttpContext.Response.StatusCode = 403;
            }
        }
    }
}