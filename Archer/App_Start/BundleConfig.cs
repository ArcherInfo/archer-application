﻿using System.Web;
using System.Web.Optimization;

namespace Archer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.


            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/Js").Include(
                  "~/Content/plugins/jquery/jquery.min.js",
                  "~/Content/plugins/bootstrap/js/bootstrap.bundle.min.js",
                  "~/Content/plugins/slimScroll/jquery.slimscroll.min.js",
                  "~/Content/plugins/fastclick/fastclick.js",
                   "~/Content/dist/js/adminlte.min.js",
                   "~/Content/plugins/knob/jquery.knob.js",
                   "~/Content/plugins/sparkline/jquery.sparkline.min.js",
                   "~/Content/plugins/datatables/jquery.dataTables.js",
                   "~/Content/plugins/Notify/js/jquery.toast.js",
                  "~/Content/plugins/datepicker/bootstrap-datepicker.js",
                    "~/Content/dist/js/demo.js"));

            bundles.Add(new StyleBundle("~/Css").Include(
                       "~/Content/plugins/datatables/jquery.dataTables.css",
                       "~/Content/plugins/Notify/css/jquery.toast.css",
                       "~/Content/plugins/jQueryUI/jquery-ui.css",
                       "~/Content/Css/bootstrap.min.css",
                       "~/Content/plugins/datepicker/datepicker3.css",
                      "~/Content/dist/css/adminlte.min.css"));

            bundles.Add(new ScriptBundle("~/UserSideModernizr").Include(
                        "~/Content/js/modernizr-2.6.2.min.js"));

            bundles.Add(new ScriptBundle("~/UserSideJs").Include(
                     "~/Content/js/jquery.min.js",
                    "~/Content/js/jquery.easing.1.3.js",
                    "~/Content/js/bootstrap.min.js",
                    "~/Content/js/jquery.waypoints.min.js",
                    "~/Content/js/jquery.stellar.min.js",
                    "~/Content/js/owl.carousel.min.js",
                    "~/Content/js/jquery.countTo.js",
                    "~/Content/js/jquery.magnific-popup.min.js",
                    "~/Content/js/magnific-popup-options.js",
                    //"~/Content/dist/js/bootstrap.bundle.min.js",
                    "~/Content/js/main.js"));

            bundles.Add(new StyleBundle("~/UserSideCss").Include(
                "~/Content/css/animate.css",
                "~/Content/css/icomoon.css",
                "~/Content/css/bootstrap.css",
                "~/Content/css/magnific-popup.css",
                "~/Content/css/owl.carousel.min.css",
                "~/Content/css/owl.theme.default.min.css",
                "~/Content/css/style.css",
                "~/Content/css/Custom.css"
               ));



        }
    }
}
