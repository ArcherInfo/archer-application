﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace Archer
{
    public class FormsAuthenticationService : IAuthenticationService
    {
        public FormsAuthenticationService()
        {
        }

        public string DefaultRedirectUrl
        {
            get { return "/Account/LoginWindow"; }
        }




        public void Login(int userId, string roles, bool rememberMe = false)
        {
            FormsAuthentication.Initialize();
            var expire = DateTime.Now.AddHours(1);
            if (rememberMe)
                expire = DateTime.Now.AddDays(14);

            var ticket = new FormsAuthenticationTicket(
                1,
                userId.ToString(),
                DateTime.Now,
                expire,
                rememberMe,
                roles
            );
            var rawCookie = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, rawCookie);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public void Logoff()
        {
            if (FormsAuthentication.IsEnabled)
                FormsAuthentication.SignOut();
        }

        public IPrincipal ProcessRequest(string ticketName)
        {
            IPrincipal principal = null;

            if (!string.IsNullOrEmpty(ticketName))
            {
                try
                {
                    var identity = new GenericIdentity(ticketName, "Forms");
                    principal = new GenericPrincipal(identity, new string[] { });
                }
                catch
                { }
            }

            return principal;
        }
    }
}