﻿// -----------------------------------------------------------------------
// <copyright file="IAuthenticationService" company="Archer">
// Copyright (c) 2017 All Rights Reserved
// </copyright>
// <author>Yogesh Patil</author>
// <date>10/07/2003 5:30:66 PM</date>
// <summary></summary>
// -----------------------------------------------------------------------

namespace Archer
{
    using System.Security.Principal;

    interface IAuthenticationService
    {
        string DefaultRedirectUrl { get; }
        void Login(int userId, string roles, bool rememberMe = false);        
        void Logoff();
        IPrincipal ProcessRequest(string ticketName);
    }
}
