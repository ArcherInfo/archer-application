﻿//-----------------------------------------------------------------------
// <copyright file="RepositoryHelper.cs" company="Archer Technologies">
//     Copyright (c) Storefloor. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Archer.Helper
{
    public static class RepositoryHelper
    {
        /// <summary>
        /// parse int for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static int GetIntParam(string param)
        {
            int val = 0;
            if (int.TryParse(param, out val))
            {
                return val;
            }

            return 0;
        }

        /// <summary>
        /// parse string for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string GetStringParam(string param)
        {
            if (!string.IsNullOrEmpty(param))
            {
                return param.Trim().ToLower();
            }
            return "";
        }

        /// <summary>
        /// parse bool for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool GetBoolParam(string param)
        {
            bool val;
            bool.TryParse(param, out val);
            return val;
        }

        /// <summary>
        /// parse nullable bool for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool? GetNullableBoolParam(string param)
        {
            bool val;
            if (bool.TryParse(param, out val))
            {
                return val;
            }

            return null;
        }

        /// <summary>
        /// parse nullable DateTime for a request form param
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static DateTime? GetNullableDate(string param)
        {
            DateTime val;
            if (DateTime.TryParse(param, out val))
            {
                return val;
            }

            return null;
        }

        /// <summary>
        /// calculate the amount of pages in resultset
        /// </summary>
        /// <param name="count">count of records from query</param>
        /// <param name="limit">record limit per page</param>
        /// <returns></returns>
        public static decimal TotalPages(int count, int limit)
        {
            return count > 0 ? Math.Ceiling(Convert.ToDecimal(count) / Convert.ToDecimal(limit)) : 0m;
        }

        /// <summary>
        /// calculate the requested page from client
        /// </summary>
        /// <param name="page"></param>
        /// <param name="totalPages"></param>
        /// <returns></returns>
        public static int Page(int page, decimal totalPages)
        {
            return page > totalPages ? Convert.ToInt32(totalPages) : page;
        }
        /// <summary>
        /// calculate starting point of requested page
        /// </summary>
        /// <returns></returns>
        public static int Start(int PageSize, int PageIndex)
        {
            return PageSize > 0 && PageIndex > 0 ? (PageIndex * PageSize) - PageSize : 0;
        }
    }
}
