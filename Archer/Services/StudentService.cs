﻿using Archer.Helper;
using Archer.Helpers;
using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class StudentService : Service
    {
        public bool AddStudent(StudentModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {

                using (ArcherEntities db = new ArcherEntities())
                {
                    var dupplicatesubject = db.tblStudents.Any(x => x.MobileNo == model.MobileNo);
                    if (dupplicatesubject) Errors.Add("Student", "Student Already Registered");
                    var path = " ";
                    if (Errors.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        string subjects = "";
                        tblStudent student = new tblStudent();
                        student.Name = model.Name;
                        student.Email = model.Email;
                        student.MobileNo = model.MobileNo;
                        student.WhatsappNo = model.WhatsappNo;
                        student.CurrentAdd = model.CurrentAdd;
                        student.PremanantAdd = model.PremanantAdd;
                        student.College = model.College;
                        student.YearTrade = model.YearTrade;
                        student.Refernce = model.Refernce;
                        student.Subject = model.Subject;
                        student.AdmissionDate = model.AdmissionDate;
                        db.tblStudents.Add(student);
                        db.SaveChanges();
                        if (model.TSelectedSubjects != null)
                        {
                            if (model.TSelectedSubjects.Count > 0)
                            {
                                foreach (int id in model.TSelectedSubjects)
                                {
                                    subjects += id + "|";
                                }
                                student.Subject = subjects;
                                db.SaveChanges();

                            }
                        }
                        //if (image != null && image.ContentLength > 0)
                        //{
                        //    var fileName = Path.GetFileName(image.FileName);
                        //    path = Path.Combine(Server.MapPath("~/Static/Images/Studentimages/" + student.StudentId), fileName);
                        //    if (!Directory.Exists(Server.MapPath("~/Static/Images/Studentimages/" + student.StudentId)))
                        //    {
                        //        Directory.CreateDirectory(Server.MapPath("~/Static/Images/Studentimages/" + student.StudentId));
                        //    }
                        //    image.SaveAs(path);
                        //    student.StudentImage = Path.GetFileName(image.FileName);
                        //    db.SaveChanges();
                        //    return true;
                        //}
                        return true;

                    }
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("StudentService| AddStudent()|" + e.ToString());
                return false;
            }
        }
        public StudentModel GetStudent(int id)
        {
            StudentModel model = new StudentModel();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblStudent student = db.tblStudents.FirstOrDefault(x => x.StudentId == id);
                    if (student != null)
                    {
                        model.StudentId = student.StudentId;
                        model.Email = student.Email;
                        model.Name = student.Name;
                        model.MobileNo = student.MobileNo;
                        model.WhatsappNo = student.WhatsappNo;
                        model.YearTrade = student.YearTrade;
                        model.College = student.College;
                        model.PremanantAdd = student.PremanantAdd;
                        model.CurrentAdd = student.CurrentAdd;
                        model.Refernce = student.Refernce;
                        model.AdmissionDate = student.AdmissionDate;
                        model.Image = student.StudentImage;
                        return (model);
                    }
                }
            }
            catch
            {
                return (model);
            }
            return (model);
        }
        public bool EditStudent(StudentModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {

                using (ArcherEntities db = new ArcherEntities())
                {

                    string subjects = "";
                    var path = " ";
                    var dupplicatesubject = db.tblStudents.Any(x => x.MobileNo == model.MobileNo && x.Email == model.Email && x.StudentId != model.StudentId);

                    if (dupplicatesubject) Errors.Add("Student", "Student Already Registered");

                    if (Errors.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        tblStudent student = db.tblStudents.FirstOrDefault(x => x.StudentId == model.StudentId);
                        if (student != null)
                        {
                            student.Name = model.Name;
                            student.Email = model.Email;
                            student.MobileNo = model.MobileNo;
                            student.WhatsappNo = model.WhatsappNo;
                            student.CurrentAdd = model.CurrentAdd;
                            student.PremanantAdd = model.PremanantAdd;
                            student.College = model.College;
                            student.YearTrade = model.YearTrade;
                            student.Refernce = model.Refernce;
                            student.Subject = model.Subject;
                            student.AdmissionDate = model.AdmissionDate;
                            db.SaveChanges();

                            if (model.TSelectedSubjects != null)
                            {
                                if (model.TSelectedSubjects.Count > 0)
                                {
                                    foreach (int id in model.TSelectedSubjects)
                                    {
                                        subjects += id + "|";
                                    }
                                    student.Subject = subjects;
                                    db.SaveChanges();
                                }
                            }
                            //if (image != null && image.ContentLength > 0)
                            //{
                            //    var fileName = Path.GetFileName(image.FileName);
                            //    path = Path.Combine(Server.MapPath("~/Static/Images/Studentimages/" + student.StudentId), fileName);
                            //    if (!Directory.Exists(Server.MapPath("~/Static/Images/Studentimages/" + student.StudentId)))
                            //    {
                            //        Directory.CreateDirectory(Server.MapPath("~/Static/Images/Studentimages/" + student.StudentId));
                            //    }
                            //    image.SaveAs(path);
                            //    student.StudentImage = Path.GetFileName(image.FileName);
                            //    db.SaveChanges();
                            //    return true;
                            //}
                            return true;
                        }
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("StudentService| EditStudent()|" + e.ToString());
                return false;
            }

        }
        public List<StudentModel> StudentList()
        {
            List<StudentModel> modellist = new List<StudentModel>();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblStudent> students = db.tblStudents.ToList();

                    if (students.Count > 0)
                    {
                        foreach (tblStudent student in students)
                        {
                            StudentModel model = new StudentModel();
                            model.StudentId = student.StudentId;
                            model.Name = student.Name;
                            model.Email = student.Email;
                            model.PremanantAdd = student.PremanantAdd;
                            model.CurrentAdd = student.CurrentAdd;
                            model.MobileNo = student.MobileNo;
                            model.WhatsappNo = student.WhatsappNo;
                            model.AdmissionDate = student.AdmissionDate;
                            model.College = student.College;
                            model.YearTrade = student.YearTrade;
                            model.Refernce = student.Refernce;
                            modellist.Add(model);
                        }
                        return (modellist);
                    }
                    return (modellist);
                }

            }
            catch (Exception e)
            {
                return (modellist);
            }
        }
        public bool deleteStudent(int? id)
        {
            try
            {

                using (ArcherEntities db = new ArcherEntities())
                {
                    tblStudent student = db.tblStudents.FirstOrDefault(x => x.StudentId == id);
                    db.tblStudents.Remove(student);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("StudentService| deleteStudent()|" + e.ToString());
                return false;
            }
        }
        public object GetStudentList(System.Collections.Specialized.NameValueCollection formParams)
        {
            var form = new GenericListParams(formParams);

            int count = 0;
            decimal totalPages = 0;
            int page = 0;
            int counter = 1;
            List<StudentModel> list = new List<StudentModel>();
            using (var db = new ArcherEntities())
            {
                // var query = db.tbl.AsQueryable();
                //var startDate = RepositoryHelper.GetNullableDate(form.Params["startDate"]);
                //var endDate = RepositoryHelper.GetNullableDate(form.Params["endDate"]);
                //if (!string.IsNullOrEmpty(form.Params["Brand"]) && form.Params["Brand"] != "-1")
                //{
                //    int brandid = int.Parse(form.Params["Brand"]);
                //    query = query.Where(x => x.Brand_Id == brandid);
                //}
                //if (!string.IsNullOrEmpty(form.Params["Category"]) && form.Params["Category"] != "-1")
                //{
                //    int categoryid = int.Parse(form.Params["Category"]);
                //    query = query.Where(x => x.tblBrand.tblCategory.Category_Id == categoryid);
                //}
                //if (!string.IsNullOrEmpty(form.Params["group"]) && form.Params["group"] != "-1")
                //{
                //    int groupid = int.Parse(form.Params["group"]);
                //    query = query.Where(x => x.tblBrand.tblGroup.GroupId == groupid);
                //}

                //if (startDate.HasValue) { query = query.Where(x => x.SaleDate >= startDate.Value); }

                //if (endDate.HasValue) { query = query.Where(x => x.SaleDate <= endDate.Value); }

                //count = query.Count();

                //query = query.OrderBy(x => x.Id).Skip(form.Start).Take(form.Limit);

                List<tblStudent> stds = db.tblStudents.ToList();
                if (stds.Count > 0)
                {
                    foreach (var student in stds)
                    {
                        
                        StudentModel model = new StudentModel();
                        model.StudentId = student.StudentId;
                        model.Name = student.Name ;
                        model.MobileNo = student.MobileNo;
                        model.WhatsappNo = student.WhatsappNo;
                        model.Subject = student.Subject;
                        model.College = student.College == null ? "-" : student.College;
                        model.CurrentAdd = student.CurrentAdd == null ? "-": student.CurrentAdd;
                        model.PremanantAdd = student.PremanantAdd == null ? "-" : student.PremanantAdd;
                        model.YearTrade = student.YearTrade;
                        model.Email = student.Email;
                        model.AdmissionDate = student.AdmissionDate;
                        list.Add(model);
                    }
                }
            }

            //calculate total no of pages
            totalPages = RepositoryHelper.TotalPages(count, form.Limit);
            page = RepositoryHelper.Page(form.Page, totalPages);

            //put the serial number to records
            list.ForEach(x => x.srno = counter++);

            return new StudentPagingModel
            {
                page = page,
                total = totalPages,
                records = count,
                data = list
            };
        }
    }
}