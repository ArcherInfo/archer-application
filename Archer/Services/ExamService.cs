﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class ExamService
    {
        public bool AddExam(ExamModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblExam test = new tblExam();
                    test.Info = model.info;
                    
                    db.tblExams.Add(test);
                    db.SaveChanges();
                    var path = " ";
                    if (image != null && image.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(image.FileName);
                        path = Path.Combine(Server.MapPath("~/Static/Images/ExamImages/" + test.id), fileName);
                        if (!Directory.Exists(Server.MapPath("~/Static/Images/ExamImages/" + test.id)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/Static/Images/ExamImages/" + test.id));
                        }
                        image.SaveAs(path);
                        test.ImageName = Path.GetFileName(image.FileName);
                        db.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ExamService| AddExam()|" + e.ToString());
                return false;
            }
        }
        public ExamModel GetExam(int id)
        {
            ExamModel model = new ExamModel();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblExam exam = db.tblExams.FirstOrDefault(x => x.id == id);
                    if (exam != null)
                    {
                        model.info = exam.Info;
                        model.id = exam.id;
                        model.img = exam.ImageName;
                        return (model);
                    }
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ExamService| GetExam()|" + e.ToString());

            }
            return (model);
        }
        public bool EditExam(ExamModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    // TODO: Add update logic here
                    tblExam test = db.tblExams.FirstOrDefault(x => x.id == model.id);
                    if (test != null)
                    {
                        test.Info = model.info;
                         
                        var path = " ";
                        if (image != null && image.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(image.FileName);
                            path = Path.Combine(Server.MapPath("~/Static/Images/ExamImages/" + test.id), fileName);
                            if (!Directory.Exists(Server.MapPath("~/Static/Images/ExamImages/" + test.id)))
                            {
                                Directory.CreateDirectory(Server.MapPath("~/Static/Images/ExamImages/" + test.id));
                            }
                            image.SaveAs(path);
                            test.ImageName = Path.GetFileName(image.FileName);
                            db.SaveChanges();
                        }
                        db.SaveChanges();
                        return true;
                    }
                    return true;
                }
               
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ExamService| EditExam()|" + e.ToString());
                return false;
            }
            return true;
        }
        public List<ExamModel> ExamList()
        {
            List<ExamModel> modellist = new List<ExamModel>();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblExam> exams = db.tblExams.ToList();
                    
                    if (exams.Count > 0)
                    {
                        foreach (tblExam exam in exams)
                        {
                            ExamModel model = new ExamModel();
                            model.id = exam.id;
                            model.info = exam.Info;
                            model.img = exam.ImageName;
                            modellist.Add(model);
                        }
                    }
                    return (modellist);
                }
            }
            catch(Exception e)
            {
                return (modellist);
            }
        }
        public bool deleteExam(int? id)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblExam exam = db.tblExams.FirstOrDefault(x => x.id == id);
                    db.tblExams.Remove(exam);
                    db.SaveChanges();
                    return true;
                }
                return true;
            }
            catch(Exception e)
            {
                CommonFuntions.LogMessage("Examservice| deleteExam()|" + e.ToString());
                return false;
            }
        }
    }
}