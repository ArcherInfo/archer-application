﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class OnlineCourseService : Service
    {
        public object ViewBag { get; private set; }
        public bool AddCourse(OnlineCourcesModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblOnlineCourse cource = new tblOnlineCourse();
                    cource.Info = model.Info;
                    cource.Postedon = model.Postedon;
                    cource.Name = model.Name;
                    cource.ratings = model.Ratings;
                    db.tblOnlineCourses.Add(cource);
                    db.SaveChanges();
                    var path = " ";
                    if (image != null && image.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(image.FileName);
                        path = Path.Combine(Server.MapPath("~/Static/Images/CourcesImages/" + cource.id), fileName);
                        if (!Directory.Exists(Server.MapPath("~/Static/Images/CourcesImages/" + cource.id)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/Static/Images/CourcesImages/" + cource.id));
                        }
                        image.SaveAs(path);
                        cource.Image = Path.GetFileName(image.FileName);
                        db.SaveChanges();
                        return true;
                    }
                }
                return false;

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("OnlineCourseService| AddCourse()|" + e.ToString());
                return false;
            }
        }
        public OnlineCourcesModel GetCourse(int id)
        {
            OnlineCourcesModel model = new OnlineCourcesModel();

            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblOnlineCourse cource = db.tblOnlineCourses.FirstOrDefault(x => x.id == id);
                    if (cource != null)
                    {
                        model.id = cource.id;
                        model.Info = cource.Info;
                        model.Postedon = cource.Postedon;
                        model.Image = cource.Image;
                        cource.Name = model.Name;
                        cource.ratings = model.Ratings;
                        return (model);
                    }
                }
            }

            catch (Exception e)
            {
                CommonFuntions.LogMessage("OnlineCourseService| GetCourse()|" + e.ToString());

            }
            return (model);
        }
        public bool EditCourse(OnlineCourcesModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblOnlineCourse cource = db.tblOnlineCourses.FirstOrDefault(x => x.id == model.id);
                    if (cource != null)
                    {
                        cource.Info = model.Info;
                        cource.Postedon = model.Postedon;
                        cource.Name = model.Name;
                        cource.ratings = model.Ratings;
                        db.SaveChanges();
                        var path =" ";
                        if (image != null && image.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(image.FileName);
                            path = Path.Combine(Server.MapPath("~/Static/Images/CourcesImages/" + cource.id), fileName);
                            if (!Directory.Exists(Server.MapPath("~/Static/Images/CourcesImages/" + cource.id)))
                            {
                                Directory.CreateDirectory(Server.MapPath("~/Static/Images/CourcesImages/" + cource.id));
                            }
                            image.SaveAs(path);
                            cource.Image = Path.GetFileName(image.FileName);
                            db.SaveChanges();
                            return true;
                        }
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("OnlineCourseService| EditCourse()|" + e.ToString());
                return false;
            }
            return true;
        }
        public List<OnlineCourcesModel> CourseList()
        {
            List<OnlineCourcesModel> modellist = new List<OnlineCourcesModel>();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblOnlineCourse> cources = db.tblOnlineCourses.ToList();
                    
                    if (cources.Count > 0)
                    {
                        foreach (tblOnlineCourse cource in cources)
                        {
                            OnlineCourcesModel model = new OnlineCourcesModel();
                            model.id = cource.id;
                            model.Postedon = cource.Postedon;
                            model.Info = cource.Info;
                            model.Image = cource.Image;
                            model.Name = cource.Name;
                            model.Ratings = cource.ratings;
                            modellist.Add(model);
                        }
                        return (modellist);
                    }
                    return (modellist);
                }
            }
            catch (Exception e)
            {
                return (modellist);
            }
        }
        public bool deleteCourse(int? id)
        {
            try
            {

                using (ArcherEntities db = new ArcherEntities())
                {
                    tblOnlineCourse cource = db.tblOnlineCourses.FirstOrDefault(x => x.id == id);
                    db.tblOnlineCourses.Remove(cource);
                    db.SaveChanges();
                    return true;
                }
                return true;
            }
            catch(Exception e)
            {
                CommonFuntions.LogMessage("OnlineCourseService| deleteCourse()|" + e.ToString());
                return false;
            }
        }
    }
}