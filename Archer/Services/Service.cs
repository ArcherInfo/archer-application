﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class Service
    {
        public Dictionary<string, string> Errors { get; set; }
        public Service()
        {
            Errors = new Dictionary<string, string>();
        }
    }
}