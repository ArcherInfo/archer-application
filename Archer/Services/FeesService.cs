﻿using Archer.Helper;
using Archer.Helpers;
using Archer.Models;
using Archer.Models.sql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class FeesService : Service
    {
        public object GetFeesList(System.Collections.Specialized.NameValueCollection formParams)
        {
            var form = new GenericListParams(formParams);

            int count = 0;
            decimal totalPages = 0;
            int page = 0;
            int counter = 1;
            int subjectid = 0;
            List<FeesModel> modellist = new List<FeesModel>();
            using (var db = new ArcherEntities())
            {
                var query = db.tblStudents.AsQueryable();
                var startDate = RepositoryHelper.GetNullableDate(form.Params["startDate"]);
                var endDate = RepositoryHelper.GetNullableDate(form.Params["endDate"]);
                if (!string.IsNullOrEmpty(form.Params["Sub"]) && form.Params["Sub"] != "-1")
                {
                    subjectid = int.Parse(form.Params["Sub"]);
                }
                if (startDate.HasValue) { query = query.Where(x => x.AdmissionDate >= startDate.Value); }
                // if (endDate.HasValue) { query = query.Where(x => EntityFunctions.TruncateTime(x.Created) <= EntityFunctions.TruncateTime(endDate.Value)); }

                if (endDate.HasValue) { query = query.Where(x => x.AdmissionDate <= endDate.Value); }
                count = query.Count();
                query = query.OrderBy(x => x.StudentId).Skip(form.Start).Take(form.Limit);


            
                foreach (var student in query)
                {
                   
                    if (student != null)
                    {
                        if (!String.IsNullOrEmpty(student.Subject))
                        {
                            string allsubjects = student.Subject;
                            string[] arraySubject = allsubjects.Split('|');
                            decimal? totalfees = 0;
                            if (arraySubject != null)
                            {
                                int flag = 0;

                                string subjects = "";
                                foreach (string str in arraySubject)
                                {
                            //        subjects = "";
                                    if (str.Length != 0)
                                    {
                                        int SubjectId = Convert.ToInt32(str);
                                        if (subjectid == SubjectId)
                                        {
                                            flag = 1;
                                        }
                                        tblSubject subject = db.tblSubjects.FirstOrDefault(x => x.IdSubject == SubjectId);
                                        if (subject != null)
                                        {
                                            subjects += subject.Name + ",";
                                            totalfees += subject.Fees;
                                        }
                                    }
                                }
                                List<tblFee> fees = db.tblFees.Where(x => x.Student == student.StudentId).ToList();
                                if (fees != null)
                                {
                                    if (flag == 1)
                                    {
                                        decimal? paidamount = 0;
                                        foreach (tblFee fee in fees)
                                        {
                                            paidamount = paidamount + fee.PaidAmount;
                                        }
                                        FeesModel fmodel = new FeesModel();
                                        if (student.AdmissionDate != null)
                                        {
                                            fmodel.datestr = student.AdmissionDate.Value.ToString("dd/MMM/yyyy");
                                        }
                                        else
                                            fmodel.datestr = "";
                                        fmodel.paidamount = paidamount;
                                        fmodel.remainingfees = totalfees- paidamount;
                                        fmodel.studentnm = student.Name;
                                        fmodel.totalFee = totalfees;
                                        fmodel.Subject = subjects;
                                        modellist.Add(fmodel);
                                    }
                                    else
                                    {
                                        decimal? paidamount = 0;
                                        foreach (tblFee fee in fees)
                                        {
                                            paidamount = paidamount + fee.PaidAmount;
                                        }
                                        FeesModel fmodel = new FeesModel();
                                        if (student.AdmissionDate != null)
                                        {
                                            fmodel.datestr = student.AdmissionDate.Value.ToString("dd/MMM/yyyy");
                                        }
                                        else
                                            fmodel.datestr = "";
                                        fmodel.paidamount = paidamount;
                                        fmodel.remainingfees = totalfees - paidamount;
                                        fmodel.studentnm = student.Name;
                                        fmodel.totalFee = totalfees;
                                        fmodel.Subject = subjects;
                                        modellist.Add(fmodel);
                                    }
                                }
                                else
                                {
                                    FeesModel fmodel = new FeesModel();
                                    fmodel.paidamount = 0;
                                    if (student.AdmissionDate != null)
                                    {
                                        fmodel.datestr = student.AdmissionDate.Value.ToString("dd/MMM/yyyy");
                                    }
                                    else
                                        fmodel.datestr = "";
                                    fmodel.remainingfees = totalfees;
                                    fmodel.studentnm = student.Name;
                                    fmodel.totalFee = totalfees;
                                    fmodel.Subject = subjects;
                                    modellist.Add(fmodel);
                                }
                            }
                        }
                    }
                }
            }

            //calculate total no of pages
            totalPages = RepositoryHelper.TotalPages(count, form.Limit);
            page = RepositoryHelper.Page(form.Page, totalPages);

            //put the serial number to records
            modellist.ForEach(x => x.SrNo = counter++);

            return new FeesPagingModel
            {
                page = page,
                total = totalPages,
                records = count,
                data = modellist
            };
        }
    }
}