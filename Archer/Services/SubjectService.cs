﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class SubjectService:Service
    {
        
        public bool AddSubject(SubjectModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    var dupplicatesubject = db.tblSubjects.Any(x => x.Name == model.name);
                    if (dupplicatesubject) Errors.Add("Subject", "Subject Has Already available");
                    if (Errors == null || Errors.Count == 0)
                    {
                        tblSubject form = new tblSubject();
                        form.Name = model.name;
                        form.Fees = model.fees;
                        db.tblSubjects.Add(form);
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("SubjectService| AddSubject()|" + e.ToString());
                return false;
            }
        }
        public SubjectModel Getsubject(int id)
        {
            SubjectModel model = new SubjectModel();

            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblSubject test = db.tblSubjects.FirstOrDefault(x => x.IdSubject == id);

                    if (test != null)
                    {
                        model.id = test.IdSubject;
                        model.name = test.Name;
                        model.fees = (Int32)test.Fees;
                        return (model);
                    }

                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("SubjectService| Getsubject()|" + e.ToString());

            }
            return (model);
        }
        public bool EditSubject(SubjectModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {

                    var dupplicatesubject = db.tblSubjects.Any(x => x.Name == model.name && x.IdSubject!=model.id);

                    if (dupplicatesubject) Errors.Add("Subject", "Subject Has Already available");
                    if (Errors.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        tblSubject test = db.tblSubjects.FirstOrDefault(x => x.IdSubject == model.id);
                        if (test != null)
                        {
                            test.Name = model.name;
                            test.Fees = model.fees;
                            db.SaveChanges();
                            return true;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("SubjectService| EditSubject()|" + e.ToString());
                return false;
            }
            return true;
        }
        public List<SubjectModel> SubjectList()
        {
            List<SubjectModel> modellist = new List<SubjectModel>();
            using (ArcherEntities db = new ArcherEntities())
            {
                List<tblSubject> tests = db.tblSubjects.ToList();
                
                if (tests.Count > 0)
                {
                    foreach (tblSubject test in tests)
                    {
                        SubjectModel model = new SubjectModel();
                        model.id = test.IdSubject;
                        model.name = test.Name;
                        model.fees = (Int32)test.Fees;
                        modellist.Add(model);
                    }
                    return (modellist);
                }
                return (modellist);
            }
            return (modellist);
        }
        public bool DeleteSubject(int id)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblSubject test = db.tblSubjects.FirstOrDefault(x => x.IdSubject == id);
                    if (test != null)
                    {
                        db.tblSubjects.Remove(test);
                        db.SaveChanges();
                        return true;
                    }
                    return true;
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("SubjectService| DeleteSubject()|" + e.ToString());
                return false;
            }
        }
    }
}