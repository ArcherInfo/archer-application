﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class TrainingService
    {
        public bool AddTraining(TrainingModel model)
        {
            try
            {
                using(ArcherEntities ds = new ArcherEntities())
                {

                    tblTraining form = new tblTraining();
                    form.Trainingon = model.dt;
                    form.Trainingwhere = model.place;
                    form.TrainingSubject = model.subject;
                    ds.tblTrainings.Add(form);
                    ds.SaveChanges();
                    return true;

                }
                return false;
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TrainingService| AddTraining()|" + e.ToString());
                return false;
            }
        }
        public TrainingModel Gettraining(int id)
        {
            TrainingModel tre = new TrainingModel();

            try
            {
                using (ArcherEntities asb = new ArcherEntities())
                {
                    tblTraining enqury = asb.tblTrainings.FirstOrDefault(x => x.id == id);
                    
                    tre.id = enqury.id;
                    tre.dt = enqury.Trainingon;
                    tre.place = enqury.Trainingwhere;
                    tre.subject = enqury.TrainingSubject;
                    asb.SaveChanges();
                    return (tre);
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TrainingService| Gettraining()|" + e.ToString());

            }
            return (tre);
        }
        public bool EditTraining(TrainingModel model)
        {
            try
            {
                using (ArcherEntities arc = new ArcherEntities())
                {
                    tblTraining enqury = arc.tblTrainings.FirstOrDefault(x => x.id == model.id);
                    enqury.Trainingon = model.dt;
                    enqury.Trainingwhere = model.place;
                    enqury.TrainingSubject = model.subject;
                    arc.SaveChanges();
                }
            }

            
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TrainingService| EditTraining()|" + e.ToString());
                return false;
            }
            return true;
        }
        public List<TrainingModel> TrainingList()
        {
            List<TrainingModel> ar = new List<TrainingModel>();
            using (ArcherEntities ds = new ArcherEntities())
            {
                List<tblTraining> fs = ds.tblTrainings.ToList();
                
                foreach (tblTraining en in fs)
                {
                    TrainingModel ams = new TrainingModel();
                    ams.id = en.id;
                    //ams.dt = en.Trainingon;
                    ams.place = en.Trainingwhere;
                    ams.subject = en.TrainingSubject;
                    ams.Datestr = en.Trainingon.ToString("dd/MMM/yyyy");
                    ar.Add(ams);

                }
                return (ar);
            }
            return (ar);
        }
        public bool DeleteTraining(int id)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblTraining tds = db.tblTrainings.FirstOrDefault(x => x.id == id);
                    db.tblTrainings.Remove(tds);
                    db.SaveChanges();
                    return true;
                }
                return true;
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TrainingService| DeleteTraining()|" + e.ToString());
                return false;
            }
        }
    }
}