﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class ChannelService : Service
    {
        public object ViewBag { get; private set; }

        public bool AddChannel(ChannelModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    //var dupplicatemember = db.tblRegistrations.Any(x => x.Mob_No == model.contactno || x.EmailId == model.email);
                    //if (dupplicatemember) Errors.Add("User", "User already exists With This Mobile No. Please enter a different Mobile No.or EmailId.");
                    if (Errors == null || Errors.Count == 0)
                    {


                        tblYoutube channel = new tblYoutube();
                        channel.Title = model.Title;
                        channel.subject = model.Subject;
                        channel.YouTube_Link = model.YouTube_Link;
                        db.tblYoutubes.Add(channel);
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ChannelService| AddUser()|" + e.ToString());
                return false;
            }
        }
        public ChannelModel Getchannel(int id)
        {
            ChannelModel model = new ChannelModel();

            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblYoutube channel = db.tblYoutubes.FirstOrDefault(x => x.id == id);
                    if (channel != null)
                    {
                        model.id = channel.id;
                        model.Title = channel.Title;
                        model.YouTube_Link = channel.YouTube_Link;
                        model.Subject = channel.subject;
                        return (model);
                    }
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ChannelService| Getchannel()|" + e.ToString());

            }
            return (model);
        }
        public bool EditUser(ChannelModel model)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblYoutube channel = db.tblYoutubes.FirstOrDefault(x => x.id == model.id);
                    if (channel != null)
                    {
                        channel.Title = model.Title;
                        channel.subject = model.Subject;
                        channel.YouTube_Link = model.YouTube_Link;
                        db.SaveChanges();
                        return true;
                    }
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ChannelServices| EditUser()|" + e.ToString());
                return false;
            }
            return true;
        }
        public List<ChannelModel> ChannelList()
        {
            using (ArcherEntities db = new ArcherEntities())
            {
                List<tblYoutube> channels = db.tblYoutubes.ToList();
                List<ChannelModel> modellist = new List<ChannelModel>();
                if (channels.Count > 0)
                {
                    foreach (tblYoutube channel in channels)
                    {
                        ChannelModel model = new ChannelModel();
                        model.id = channel.id;
                        model.Title = channel.Title;
                        model.YouTube_Link = channel.YouTube_Link;
                        model.Subject = channel.subject;

                        modellist.Add(model);
                    }

                    return (modellist);
                }
                return (modellist);
            }
        }
        public bool DeleteChannel(int id)
        {
            try
            {
                using (var db = new ArcherEntities())
                {
                    tblYoutube channel = db.tblYoutubes.FirstOrDefault(x => x.id == id);
                    if (channel != null)
                    {
                        db.tblYoutubes.Remove(channel);
                        db.SaveChanges();
                        return true;
                    }
                    return true;
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ChannelService| DeleteChannel()|" + e.ToString());
                return false;
            }
        }
    }
}