﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class ProjectService
    {
        public bool AddProject(ProjectModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblProject project = new tblProject();
                    project.Title = model.Title;
                    project.Heading = model.Heading;
                    project.Praraghraph = model.Praraghraph;
                    db.tblProjects.Add(project);
                    db.SaveChanges();
                    var path = " ";
                    if (image != null && image.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(image.FileName);
                        path = Path.Combine(Server.MapPath("~/Static/Images/ProjectImages/" + project.id), fileName);
                        if (!Directory.Exists(Server.MapPath("~/Static/Images/ProjectImages/" + project.id)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/Static/Images/ProjectImages/" + project.id));
                        }
                        image.SaveAs(path);
                        project.Image = Path.GetFileName(image.FileName);
                        db.SaveChanges();
                        return true;
                    }
                }
                return false;

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ProjectService| AddProject()|" + e.ToString());
                return false;
            }
        }
        public ProjectModel GetProject(int id)
        {
            ProjectModel model = new ProjectModel();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblProject project = db.tblProjects.FirstOrDefault(x => x.id == id);
                    if (project != null)
                    {
                        model.id = project.id;
                        model.Title = project.Title;
                        model.Heading = project.Heading;
                        model.Praraghraph = project.Praraghraph;
                        model.Image = project.Image;
                        return (model);
                    }
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ProjectService| GetProject()|" + e.ToString());

            }
            return (model);
        }
        public bool EditProject(ProjectModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblProject project = db.tblProjects.FirstOrDefault(x => x.id == model.id);
                    if (project != null)
                    {
                        project.Title = model.Title;
                        project.Heading = model.Heading;
                        project.Praraghraph = model.Praraghraph;
                        db.SaveChanges();
                        var path = " ";
                        if (image != null && image.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(image.FileName);
                            path = Path.Combine(Server.MapPath("~/Static/Images/ProjectImages/" + project.id), fileName);
                            if (!Directory.Exists(Server.MapPath("~/Static/Images/ProjectImages/" + project.id)))
                            {
                                Directory.CreateDirectory(Server.MapPath("~/Static/Images/ProjectImages/" + project.id));
                            }
                            image.SaveAs(path);
                            project.Image = Path.GetFileName(image.FileName);
                            db.SaveChanges();
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ProjectService| EditProject()|" + e.ToString());
                return false;
            }
            return true;
        }
        public List<ProjectModel> ProjectList()
        {
            List<ProjectModel> modellist = new List<ProjectModel>();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblProject> projects = db.tblProjects.ToList();
                    if (projects.Count > 0)
                    {
                        foreach (tblProject project in projects)
                        {
                            ProjectModel model = new ProjectModel();
                            model.id = project.id;
                            model.Title = project.Title;
                            model.Heading = project.Heading;
                            model.Praraghraph = project.Praraghraph;
                            model.Image = project.Image;
                            modellist.Add(model);
                        }
                        return (modellist);
                    }
                    return (modellist);
                }

            }
            catch (Exception e)
            {
                return (modellist);
            }
        }
        public bool deleteProject(int? id)
        {
            try
            {

                using (ArcherEntities db = new ArcherEntities())
                {
                    tblProject project = db.tblProjects.FirstOrDefault(x => x.id == id);
                    if (project != null)
                    {
                        db.tblProjects.Remove(project);
                        db.SaveChanges();
                        return true;
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("ProjectService| deleteProject()|" + e.ToString());
                return false;
            }
        }
    }
}