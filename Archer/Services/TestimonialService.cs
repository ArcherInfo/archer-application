﻿using Archer.Models;
using Archer.Models.sql;
using Archer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Archer.Services
{
    public class TestimonialService:Service
    {
        public bool AddTestimonial(TestmonialModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    var dupplicatesubject = db.tblTestimonials.Any(x => x.Name == model.Name);

                    if (dupplicatesubject) Errors.Add("Testimonial", "Testimonial Already Registered");

                    tblTestimonial test = new tblTestimonial();
                    test.Name = model.Name;
                    test.Via = model.Via;
                    test.Description = model.Description;
                    db.tblTestimonials.Add(test);
                    db.SaveChanges();
                    var path = " ";
                    if (image != null && image.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(image.FileName);
                        path = Path.Combine(Server.MapPath("~/Static/Images/TestimonialImages"), fileName);

                        //if (!Directory.Exists(Server.MapPath("~/Static/Images/TestimonialImages/" + test.Id)))
                        //{
                        //    Directory.CreateDirectory(Server.MapPath("~/Static/Images/TestimonialImages/" + test.Id));
                        //}
                        image.SaveAs(path);
                        test.Image = Path.GetFileName(image.FileName);
                        db.SaveChanges();
                        return true;
                    }
                    return true;
                }

            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TestimonialService| AddTestimonial()|" + e.ToString());
                return false;
            }
        }
        public TestmonialModel Gettestimonail(int id)
        {
            TestmonialModel model = new TestmonialModel();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblTestimonial test = db.tblTestimonials.FirstOrDefault(x => x.Id == id);
                    
                    if (test != null)
                    {
                        model.Id = test.Id;
                        model.Via = test.Via;
                        model.Name = test.Name;
                        model.Description = test.Description;
                        model.Image = test.Image;
                        return (model);
                    }
                }
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TestimonialService| Gettestimonail()|" + e.ToString());

            }
            return (model);
        }
        public bool EditTestimonial(TestmonialModel model, HttpPostedFileBase image, HttpServerUtilityBase Server)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblTestimonial test = db.tblTestimonials.FirstOrDefault(x => x.Id == model.Id);
                    if (test != null)
                    {
                        test.Name = model.Name;
                        test.Via = model.Via;
                        test.Description = model.Description;
                        db.SaveChanges();
                        var path = " ";
                        if (image != null && image.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(image.FileName);
                            path = Path.Combine(Server.MapPath("~/Static/Images/TestimonialImages/" + test.Id), fileName);
                            if (!Directory.Exists(Server.MapPath("~/Static/Images/TestimonialImages/" + test.Id)))
                            {
                                Directory.CreateDirectory(Server.MapPath("~/Static/Images/TestimonialImages/" + test.Id));
                            }
                            image.SaveAs(path);
                            test.Image = Path.GetFileName(image.FileName);
                            db.SaveChanges();
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TestimonialService| EditTestimonial()|" + e.ToString());
                return false;
            }
            return true;
        }
        public List<TestmonialModel> TestimonialList()
        {
            List<TestmonialModel> modellist = new List<TestmonialModel>();
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    List<tblTestimonial> tests = db.tblTestimonials.ToList();
                    
                    if (tests.Count > 0)
                    {
                        foreach (tblTestimonial test in tests)
                        {
                            TestmonialModel model = new TestmonialModel();
                            model.Id = test.Id;
                            model.Via = test.Via;
                            model.Name = test.Name;
                            model.Description = test.Description;
                            model.Image = test.Image;
                            modellist.Add(model);
                        }
                        return (modellist);
                    }
                    return (modellist);
                }
            }
            catch (Exception e)
            {
                return (modellist);
            }
        }
        public bool deleteTestimonial(int? id)
        {
            try
            {
                using (ArcherEntities db = new ArcherEntities())
                {
                    tblTestimonial test = db.tblTestimonials.FirstOrDefault(x => x.Id == id);
                    if (test != null)
                    {
                        db.tblTestimonials.Remove(test);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                CommonFuntions.LogMessage("TestimonialService| deleteTestimonial()|" + e.ToString());
                return false;
            }
        }
    }
}