using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
namespace Archer.Utilities
{
    public static class CommonFuntions
    {
        public static void LogMessage(string message)
        {
            try
            {
                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                path = path + "\\Archer\\Logs";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string filename = "ArcherLog " + DateTime.Now.Month + "_" + DateTime.Now.Day + ".log";
                string finalFileName = Path.Combine(path, filename);

                File.AppendAllText(finalFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "   ]" + Thread.CurrentThread.ManagedThreadId + "]   " + message + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public static DateTime CurrentDate()
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            return (TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneInfo));
        }
        public static DateTime ConvertedDate(DateTime date)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            return (TimeZoneInfo.ConvertTimeFromUtc(date, timeZoneInfo));
        }
        public const string AdminRole = "Admin";
    }
}