﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class TrainingModel
    {
        public int id { get; set; }

        public DateTime dt { get; set; }
        public string place { get; set; }
        public string subject { get; set; }
        public string Datestr { get; set; }
    }
}