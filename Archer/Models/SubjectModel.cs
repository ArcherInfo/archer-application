﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class SubjectModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal? fees { get; set; }
             
    }
}