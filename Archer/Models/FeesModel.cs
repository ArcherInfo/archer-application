﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class FeesModel
    {
        public int id { get; set; }
        public decimal? paidamount { get; set; }
        public DateTime? date { get; set; }
        public string datestr { get; set; }
        public int student { get; set; }
        public decimal? remainingfees { get; set; }
        public string studentnm { get; set; }
        public decimal? totalFee { get; set; }
        public string Subject { get; set; }

        public List<SubjectModel> subjects { get; set; }


        public int SrNo { get; set; }
    }
    public class FeesPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public IEnumerable<FeesModel> data { get; set; }
    }

}
