﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class StudentModel
    {
        public StudentModel() { }
        public StudentModel(List<SubjectModel> subjects)
        {
            FromSubjects = subjects;
            ToSubjects = new List<SubjectModel>();
            FSelectedSubjects = new List<int>();
            TSelectedSubjects = new List<int>();
        }
        public int srno { get; set; }
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string WhatsappNo { get; set; }
        public string Email { get; set; }
        public string College { get; set; }
        public string YearTrade { get; set; }
        public string PremanantAdd { get; set; }
        public string CurrentAdd { get; set; }
        public string Refernce { get; set; }
        public string Subject { get; set; }
        public string Image { get; set; }
        public DateTime? AdmissionDate { get; set; }


        public virtual ICollection<SubjectModel> FromSubjects { get; set; }
        public virtual ICollection<SubjectModel> ToSubjects { get; set; }

        public List<int> FSelectedSubjects { get; set; }
        public List<int> TSelectedSubjects { get; set; }
    }
    public class Student_SubjectModel
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public string Name { get; set; }
    }
    public class StudentPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public IEnumerable<StudentModel> data { get; set; }
    }

}

