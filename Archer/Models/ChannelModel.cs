﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class ChannelModel
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string YouTube_Link { get; set; }

        public int? Subject { get; set; }

    }
}