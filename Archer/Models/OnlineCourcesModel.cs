﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class OnlineCourcesModel
    {
        public int id { get; set; }
        public string Postedon { get; set; }
        public string Info { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public int? Ratings { get; set; }
    }
}