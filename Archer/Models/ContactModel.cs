﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models.sql
{
    public class ContactModel
    {
        public string name { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string subject { get; set; }
        public string msg { get; set; }
    }
}