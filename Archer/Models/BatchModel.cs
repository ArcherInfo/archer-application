﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class BatchModel
    {
        public int id { get; set; }
        public string batchname { get; set; }
        public string time { get; set; }
        public int subject { get; set; }
    }
}