﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class StudentWithBatchModel
    {
        public int id { get; set; }
        public int student { get; set; }
        public int batch { get; set; }
        public string studentnm { get; set; }
        public string studentbatch { get; set; }
        public int? Subject { get; set; }
        public int? TotalFees { get; set; }
        public string SubjectName { get; set; }
        public int? RemainingFees { get; set; }
        public int? ammount { get; set; }
    }
}