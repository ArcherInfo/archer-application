﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class ProjectModel
    {
        public int id { get; set; }
        public string Heading { get; set; }
        public string Title { get; set; }
        public string Praraghraph { get; set; }
        public string Image { get; set; }
    }
}