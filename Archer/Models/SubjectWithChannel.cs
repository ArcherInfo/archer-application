﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class SubjectWithChannel
    {
        public int SubjectId { get; set; }
        public string subjectName { get; set; }
        public List<ChannelModel> channels { get; set; }
    }
}