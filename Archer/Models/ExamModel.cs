﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class ExamModel
    {
        public int id { get; set; }
        public string img{ get; set; }
        public string info { get; set; }
    }
}