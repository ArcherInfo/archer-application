﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class ArcherModels
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string add { get; set; }
        public string contact { get; set; }
        public string course { get; set; }
    }
}