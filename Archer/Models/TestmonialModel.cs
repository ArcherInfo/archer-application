﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Archer.Models
{
    public class TestmonialModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Via { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}