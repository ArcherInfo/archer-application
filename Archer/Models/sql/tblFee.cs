//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Archer.Models.sql
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblFee
    {
        public int FeesId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Student { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<decimal> RemainingAmount { get; set; }
    
        public virtual tblStudent tblStudent { get; set; }
    }
}
