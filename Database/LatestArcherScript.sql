USE [master]
GO
/****** Object:  Database [DBArcher]    Script Date: 8/3/2019 10:44:52 AM ******/
CREATE DATABASE [DBArcher]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBArcher', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER1\MSSQL\DATA\DBArcher.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DBArcher_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER1\MSSQL\DATA\DBArcher_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DBArcher] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBArcher].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBArcher] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBArcher] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBArcher] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBArcher] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBArcher] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBArcher] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBArcher] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBArcher] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBArcher] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBArcher] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBArcher] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBArcher] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBArcher] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBArcher] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBArcher] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DBArcher] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBArcher] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBArcher] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBArcher] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBArcher] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBArcher] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBArcher] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBArcher] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DBArcher] SET  MULTI_USER 
GO
ALTER DATABASE [DBArcher] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBArcher] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBArcher] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBArcher] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DBArcher] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DBArcher]
GO
/****** Object:  Table [dbo].[tblBlog]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblBlog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](max) NULL,
	[Image] [varchar](max) NULL,
	[ShortParagraph] [nvarchar](max) NULL,
	[FullParagraph] [nvarchar](max) NULL,
	[EventDate] [datetime] NULL,
	[Postedin] [varchar](max) NULL,
 CONSTRAINT [PK_tblBlog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblContact]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Subject] [varchar](50) NULL,
	[Message] [varchar](50) NULL,
 CONSTRAINT [PK_tblContact] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCourseDetail]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCourseDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Heading] [nvarchar](max) NULL,
	[Language_Heading] [nvarchar](max) NULL,
	[LangInfo] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[Course_heading] [nvarchar](max) NULL,
	[About_Course] [nvarchar](max) NULL,
	[AboutLearning_Heading] [nvarchar](max) NULL,
	[Learning_Info] [nvarchar](max) NULL,
	[Instructor_heading] [nvarchar](max) NULL,
	[Instructor_Name] [nvarchar](max) NULL,
	[Instructor_Info] [nvarchar](max) NULL,
	[Col1] [nvarchar](max) NULL,
	[Col2] [nvarchar](max) NULL,
	[Col3] [nvarchar](max) NULL,
	[Col4] [nvarchar](max) NULL,
	[Col5] [nvarchar](max) NULL,
	[Col6] [nvarchar](max) NULL,
	[Second_img] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblCourseDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblEnquiries]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEnquiries](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Address] [nvarchar](max) NULL,
	[Contact] [varchar](50) NULL,
	[Course] [varchar](50) NULL,
 CONSTRAINT [PK_enquiryform] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblExam]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblExam](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ImageName] [nvarchar](max) NULL,
	[Info] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblExam] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblFees]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFees](
	[FeesId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[Student] [int] NULL,
	[PaidAmount] [decimal](10, 2) NULL,
	[RemainingAmount] [decimal](10, 2) NULL,
 CONSTRAINT [PK_tblFees] PRIMARY KEY CLUSTERED 
(
	[FeesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblImage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Image_Name] [varchar](100) NULL,
 CONSTRAINT [PK_tblimage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblLogin]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLogin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[StudentId] [int] NULL,
 CONSTRAINT [PK_tblLogin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblOnlineCourse]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOnlineCourse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Postedon] [nvarchar](max) NULL,
	[Info] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[ratings] [int] NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblOnlineCourse] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblProject]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProject](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Heading] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Praraghraph] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblProject] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblStudent]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStudent](
	[StudentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[MobileNo] [nvarchar](max) NULL,
	[WhatsappNo] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[College] [nvarchar](max) NULL,
	[YearTrade] [nvarchar](max) NULL,
	[PremanantAdd] [nvarchar](max) NULL,
	[CurrentAdd] [nvarchar](max) NULL,
	[Refernce] [nvarchar](max) NULL,
	[Subject] [nvarchar](max) NULL,
	[StudentImage] [nvarchar](max) NULL,
	[AdmissionDate] [date] NULL,
 CONSTRAINT [PK_tblStudent] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblSubject]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSubject](
	[IdSubject] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Fees] [decimal](18, 2) NULL,
 CONSTRAINT [PK_tblSubjects] PRIMARY KEY CLUSTERED 
(
	[IdSubject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblTestimonial]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTestimonial](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Via] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblTestimonial] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblTraining]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTraining](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Trainingon] [datetime] NOT NULL,
	[Trainingwhere] [varchar](50) NULL,
	[TrainingSubject] [varchar](50) NULL,
	[Image] [varchar](50) NULL,
 CONSTRAINT [PK_tblTraining] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblVideoViewrs]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVideoViewrs](
	[VideoViewrsId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblVideoViewrs] PRIMARY KEY CLUSTERED 
(
	[VideoViewrsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblYoutube]    Script Date: 8/3/2019 10:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblYoutube](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[YouTube_Link] [nvarchar](max) NULL,
	[subject] [int] NULL,
 CONSTRAINT [PK_tblYoutube] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tblExam] ON 

INSERT [dbo].[tblExam] ([id], [ImageName], [Info]) VALUES (1, N'Tony-Stark-and-Google-Glass.jpg', N'C Language')
INSERT [dbo].[tblExam] ([id], [ImageName], [Info]) VALUES (2, N'Tony-Stark-and-Google-Glass.jpg', N'C lannguage')
SET IDENTITY_INSERT [dbo].[tblExam] OFF
SET IDENTITY_INSERT [dbo].[tblFees] ON 

INSERT [dbo].[tblFees] ([FeesId], [Date], [Student], [PaidAmount], [RemainingAmount]) VALUES (1, CAST(N'2019-07-16' AS Date), 1015, CAST(5000.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)))
INSERT [dbo].[tblFees] ([FeesId], [Date], [Student], [PaidAmount], [RemainingAmount]) VALUES (2, CAST(N'2019-07-16' AS Date), 1015, CAST(1000.00 AS Decimal(10, 2)), CAST(3000.00 AS Decimal(10, 2)))
INSERT [dbo].[tblFees] ([FeesId], [Date], [Student], [PaidAmount], [RemainingAmount]) VALUES (3, CAST(N'2019-07-16' AS Date), 1015, CAST(2000.00 AS Decimal(10, 2)), CAST(1000.00 AS Decimal(10, 2)))
INSERT [dbo].[tblFees] ([FeesId], [Date], [Student], [PaidAmount], [RemainingAmount]) VALUES (4, CAST(N'2019-07-16' AS Date), 1017, CAST(500.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)))
SET IDENTITY_INSERT [dbo].[tblFees] OFF
SET IDENTITY_INSERT [dbo].[tblLogin] ON 

INSERT [dbo].[tblLogin] ([Id], [UserName], [Password], [StudentId]) VALUES (1, N'veer', N'DCD1560AB5DAB79DB5298132258344DC3644B2A6', NULL)
INSERT [dbo].[tblLogin] ([Id], [UserName], [Password], [StudentId]) VALUES (3, N'Student', N'DCD1560AB5DAB79DB5298132258344DC3644B2A6', 1014)
SET IDENTITY_INSERT [dbo].[tblLogin] OFF
SET IDENTITY_INSERT [dbo].[tblProject] ON 

INSERT [dbo].[tblProject] ([id], [Heading], [Title], [Praraghraph], [Image]) VALUES (1, N'C Language', N'C Language', N'C Language C Language C Language', N'Yayati.jpg')
SET IDENTITY_INSERT [dbo].[tblProject] OFF
SET IDENTITY_INSERT [dbo].[tblStudent] ON 

INSERT [dbo].[tblStudent] ([StudentId], [Name], [MobileNo], [WhatsappNo], [Email], [College], [YearTrade], [PremanantAdd], [CurrentAdd], [Refernce], [Subject], [StudentImage], [AdmissionDate]) VALUES (1011, N'Sudeep Kamble', N'8055065600', N'805565600', N'sudeepkamble03@gmail.com', N'Waalchand Collge  Of Engineering Sangli', N'2015IT', N'kavathe Piran', N'kavathe Piran', N'Friend', N'3|', NULL, NULL)
INSERT [dbo].[tblStudent] ([StudentId], [Name], [MobileNo], [WhatsappNo], [Email], [College], [YearTrade], [PremanantAdd], [CurrentAdd], [Refernce], [Subject], [StudentImage], [AdmissionDate]) VALUES (1013, N'Student', N'9850678451', N'9850678154', N'archerdsfskdjfgh@gmail.com', N'walchand', N'2015', N'kjshajhsa', N'dskjhfkj', N'hsajdh', N'1|', NULL, CAST(N'2019-07-16' AS Date))
INSERT [dbo].[tblStudent] ([StudentId], [Name], [MobileNo], [WhatsappNo], [Email], [College], [YearTrade], [PremanantAdd], [CurrentAdd], [Refernce], [Subject], [StudentImage], [AdmissionDate]) VALUES (1014, N'student', N'4544545645', N'4564545454', N'dfsdf@gmail.com', N'dsgdfs', N'dsfgds', N'dfndsf', N'dxfdsf', N'hkjdsgfg', N'1|', NULL, CAST(N'2019-07-16' AS Date))
INSERT [dbo].[tblStudent] ([StudentId], [Name], [MobileNo], [WhatsappNo], [Email], [College], [YearTrade], [PremanantAdd], [CurrentAdd], [Refernce], [Subject], [StudentImage], [AdmissionDate]) VALUES (1015, N'archana mane', N'8624844609', NULL, N'mbbbmbm,b', N'nbbmb', N'bm,bmbmbm', N'bm,bbm,b', N'bbmbmb', N'bm,bm,b', N'1|3|', NULL, CAST(N'2019-07-01' AS Date))
INSERT [dbo].[tblStudent] ([StudentId], [Name], [MobileNo], [WhatsappNo], [Email], [College], [YearTrade], [PremanantAdd], [CurrentAdd], [Refernce], [Subject], [StudentImage], [AdmissionDate]) VALUES (1016, N'akash mane', N'8275564709', NULL, N'hjhkjhjh', N'hkjhjhjh', N'hjkhkjhj', N'hkjhkjh', N'hjkhkjhjkh', N'hjkhkjhjh', N'4|', NULL, CAST(N'2019-07-10' AS Date))
INSERT [dbo].[tblStudent] ([StudentId], [Name], [MobileNo], [WhatsappNo], [Email], [College], [YearTrade], [PremanantAdd], [CurrentAdd], [Refernce], [Subject], [StudentImage], [AdmissionDate]) VALUES (1017, N'tejas kabade', N'8275016609', NULL, N'ruyryryur', N'ytyryru', N'yruyruyry', N'ruyryruyrr y', N'ryryrruyryur', N'ryruyrfghbv', N'1|', NULL, CAST(N'2019-07-17' AS Date))
SET IDENTITY_INSERT [dbo].[tblStudent] OFF
SET IDENTITY_INSERT [dbo].[tblSubject] ON 

INSERT [dbo].[tblSubject] ([IdSubject], [Name], [Fees]) VALUES (1, N'C language', CAST(4500.00 AS Decimal(18, 2)))
INSERT [dbo].[tblSubject] ([IdSubject], [Name], [Fees]) VALUES (3, N'Java', CAST(4500.00 AS Decimal(18, 2)))
INSERT [dbo].[tblSubject] ([IdSubject], [Name], [Fees]) VALUES (4, N'Java 2015', CAST(4700.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[tblSubject] OFF
SET IDENTITY_INSERT [dbo].[tblTestimonial] ON 

INSERT [dbo].[tblTestimonial] ([Id], [Name], [Via], [Description], [Image]) VALUES (1, N'Sudeep', N'Facebbok', N'Good Class in Sangli', N'Yayati.jpg')
INSERT [dbo].[tblTestimonial] ([Id], [Name], [Via], [Description], [Image]) VALUES (2, N'Keshav Kumbhar', N'Facebook', N'Teachers having excellent teaching skill and knowledge ...', N'Mrutunjay.jpg')
SET IDENTITY_INSERT [dbo].[tblTestimonial] OFF
SET IDENTITY_INSERT [dbo].[tblTraining] ON 

INSERT [dbo].[tblTraining] ([id], [Trainingon], [Trainingwhere], [TrainingSubject], [Image]) VALUES (2, CAST(N'2018-12-30 00:00:00.000' AS DateTime), N'Sangli', N'Java', NULL)
SET IDENTITY_INSERT [dbo].[tblTraining] OFF
SET IDENTITY_INSERT [dbo].[tblYoutube] ON 

INSERT [dbo].[tblYoutube] ([id], [Title], [YouTube_Link], [subject]) VALUES (1, N'C LanguageC LanguageC LanguageC Language', N'C LanguageC LanguageC LanguageC Language', 1)
SET IDENTITY_INSERT [dbo].[tblYoutube] OFF
ALTER TABLE [dbo].[tblFees]  WITH CHECK ADD  CONSTRAINT [FK_tblFees_tblStudent1] FOREIGN KEY([Student])
REFERENCES [dbo].[tblStudent] ([StudentId])
GO
ALTER TABLE [dbo].[tblFees] CHECK CONSTRAINT [FK_tblFees_tblStudent1]
GO
ALTER TABLE [dbo].[tblLogin]  WITH CHECK ADD  CONSTRAINT [FK_tblLogin_tblStudent] FOREIGN KEY([StudentId])
REFERENCES [dbo].[tblStudent] ([StudentId])
GO
ALTER TABLE [dbo].[tblLogin] CHECK CONSTRAINT [FK_tblLogin_tblStudent]
GO
ALTER TABLE [dbo].[tblYoutube]  WITH CHECK ADD  CONSTRAINT [FK_tblYoutube_tblSubject] FOREIGN KEY([subject])
REFERENCES [dbo].[tblSubject] ([IdSubject])
GO
ALTER TABLE [dbo].[tblYoutube] CHECK CONSTRAINT [FK_tblYoutube_tblSubject]
GO
USE [master]
GO
ALTER DATABASE [DBArcher] SET  READ_WRITE 
GO
